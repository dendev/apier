<?php

namespace Dendev\Apier\Database\Factories;


use Illuminate\Database\Eloquent\Factories\Factory;

class ApiSubscriberFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'contact_name' => $this->faker->firstName() . ' ' . $this->faker->lastName(),
            'contact_email' => $this->faker->email(),
            'contact_phone' => $this->faker->phoneNumber(),
            'description' => $this->faker->text(),
            //
        ];
    }
}
