<?php

namespace Dendev\Apier;

use Dendev\Apier\Models\ApiSubscriber;
use Dendev\Apier\Notifications\ApiSubscriberTokenSet;
use \Dendev\Apier\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Apier
{
    use UtilService;

    public function create_with_array($values)
    {
        return $this->create(
            $values['name'],
            $values['contact_name'],
            $values['contact_email'],
            $values['contact_phone'],
            $values['description'],
        );
    }

    public function create($name, $contact_name, $contact_email, $contact_phone, $description = false)
    {
        $already_exist  = ApiSubscriber::where('name', $name)->first();
        if( ! $already_exist )
            $subscriber = new ApiSubscriber();
        else
            $subscriber = $already_exist;

        $subscriber->name = $name;
        $subscriber->contact_name = $contact_name;
        $subscriber->contact_email = $contact_email;
        $subscriber->contact_phone = $contact_phone;
        $subscriber->description = $description;

        $subscriber->save();

        return $subscriber;
    }

    public function create_with_token($name, $contact_name, $contact_email, $contact_phone, $description = false, $abilities = false, $send_notify = false )
    {
        $subscriber = $this->create($name, $contact_name, $contact_email, $contact_phone, $description);
        $token = $this->create_token_for_subscriber($subscriber, $abilities, $send_notify);

        if( ! $subscriber || ! $token )
        {
            Log::stack(['stack', 'api'])->error("[ApiSubscriberManagerService:create_with_token] ASMScwt01 Subscriber or token not created", [
                'subscriber_id' => $subscriber->id,
                'token' => $token
            ]);
        }
        else
        {
            $subscriber->token_as_string = $token->plainTextToken;
        }

        return $subscriber;
    }

    public function create_token_for_subscriber($id_or_model, $abilities = false, $send_notif = false)
    {
        $token = false;

        $subscriber = $this->_instantiate_if_id($id_or_model, ApiSubscriber::class);
        if( $subscriber)
        {
            $token_already_exist = $subscriber->has_token;
            if ($token_already_exist )
            {
                Log::stack(['stack', 'api'])->info("[ApiSubscriberManagerService:create_token_for_subscriber] ASMSctfs02 : Token already exist", [
                    'subscriber_id' => $subscriber->id,
                ]);
            }
            else
            {
                $token_name = Str::slug($subscriber->name ) . '_api_subscriber';

                // create token
                $available_abilities = [];
                if( $abilities ) // specific abilities
                {
                    // check abilities exist
                    $refs = config('apier.api_subscriber.abilities');
                    foreach( $abilities as $ability)
                    {
                        $ability = strtolower($ability);

                        if( in_array($ability, $refs))
                            $available_abilities[] = $ability;
                    }

                    $token = $subscriber->createToken($token_name, ['update']);
                }
                else // all abilities
                {
                    $token = $subscriber->createToken($token_name);
                }

                // notify
                if( $send_notif )
                {
                    $subscriber->abilities_as_string = ( count($available_abilities) > 0 )  ? implode(',', $available_abilities): '*';
                    $subscriber->token_as_string = $token->plainTextToken;
                    $subscriber->token_name = $token_name;
                    $subscriber->notify(new ApiSubscriberTokenSet());
                }
            }
        }
        else
        {
            Log::stack(['stack', 'api'])->error("[ApiSubscriberManagerService:create_token_for_subscriber] ASMS:stfs01 Subscriber not found", []);
        }

        return $token;
    }

    public function remove_token($id_or_model): false|ApiSubscriber
    {
        $subscriber = $this->_instantiate_if_id($id_or_model, ApiSubscriber::class);
        if( $subscriber)
        {
            if( $subscriber->has_token)
            {
                $subscriber->tokens()->delete();
                $subscriber->refresh();
            }
            else
            {
                Log::stack(['stack', 'api'])->info("[ApiSubscriberManager::remove_token] ASMrmt01: ApiSubscriber you can't delete token. Apisubscriber has no token", [
                    'subscriber' => $subscriber,
                ]);
            }

        }
        else
        {
            Log::stack(['stack', 'api'])->error("[ApiSubscriberManager::remove_token] ASMrmt01: ApiSubscriber not found", [
                'id_or_model' => $id_or_model,
            ]);
        }

        return $subscriber;
    }

    public function renew_token($id_or_model, $abilities = false, $send_notif = false ): bool|Model
    {
        $subscriber = $this->_instantiate_if_id($id_or_model, ApiSubscriber::class);
        if( $subscriber)
        {
            if( $subscriber->has_token)
                $this->remove_token($subscriber);

            $token = $this->create_token_for_subscriber($subscriber, $abilities, $send_notif );
            $subscriber->token_as_string = $token->plainTextToken;
        }
        else
        {
            Log::stack(['stack', 'api'])->error("[ApiSubscriberManager::renew_token] ASMrt01: ApiSubscriber not found", [
                'id_or_model' => $id_or_model,
            ]);
        }


        return $subscriber;
    }

    public function test_me(): string
    {
        return 'apier';
    }
}
