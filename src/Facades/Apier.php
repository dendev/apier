<?php

namespace Dendev\Apier\Facades;

use Illuminate\Support\Facades\Facade;

class Apier extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'apier';
    }
}
