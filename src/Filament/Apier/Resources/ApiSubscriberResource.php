<?php

namespace Dendev\Apier\Filament\Apier\Resources;

use Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages;
use Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\RelationManagers;
use Dendev\Apier\Models\ApiSubscriber;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\CheckboxList;
use Filament\Forms\Components\Section;
use Filament\Notifications\Notification;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Filament\Tables\Actions\Action;

class ApiSubscriberResource extends Resource
{
    protected static ?string $model = ApiSubscriber::class;

    protected static ?string $navigationIcon = 'heroicon-o-key';

    public static function form(Form $form): Form
    {
        $abilities = config('simple_planning.api_subscriber.abilities');
        $keys = array_values($abilities);
        $abilities = array_combine($keys, $abilities);

        return $form
            ->schema([
                Forms\Components\TextInput::make('name')
                    ->required()
                    ->maxLength(255),

                Forms\Components\Fieldset::make('contact')
                    ->schema([
                        Forms\Components\TextInput::make('contact_name')
                            ->required()
                            ->maxLength(255),
                        Forms\Components\TextInput::make('contact_email')
                            ->email()
                            ->required()
                            ->maxLength(255),
                        Forms\Components\TextInput::make('contact_phone')
                            ->tel()
                            ->maxLength(255),
                    ]),

                Forms\Components\Textarea::make('description')
                    ->maxLength(65535)
                    ->columnSpan('full'),

                Section::make('Token')
                    ->description('infos for create token')
                    ->schema([
                        Checkbox::make('has_token')
                            ->inline()
                            ->disabled(true)
                            ->hiddenOn(\Dendev\Apier\Filament\Resources\ApiSubscriberResource\Pages\CreateApiSubscriber::class),

                        CheckboxList::make('abilities')
                            ->options($abilities)
                            ->hiddenOn(\Dendev\Apier\Filament\Resources\ApiSubscriberResource\Pages\EditApiSubscriber::class),
                    ])
            ]);
    }

    public static function table(Table $table): Table
    {
        $abilities = 'apier.api_subscriber.abilities';
        $keys = is_array($abilities) ? array_values($abilities) : [];
        $abilities = array_combine($keys, $abilities);

        return $table
            ->columns([
                Tables\Columns\TextColumn::make('id')->sortable()->searchable(),
                Tables\Columns\TextColumn::make('name')->sortable()->searchable()->label(__('auth.failed')),
                Tables\Columns\TextColumn::make('contact_name')->sortable()->searchable(),
                Tables\Columns\TextColumn::make('created_at')->sortable()->searchable()
                    ->dateTime('d/m/Y H:s:i'),
                Tables\Columns\BooleanColumn::make('has_token'),
            ])
            ->filters([
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                Tables\Actions\ViewAction::make(),

                Action::make(__('admin/api_subscriber.action_renew_label'))
                    ->color('secondary')
                    ->icon('heroicon-o-arrow-circle-up')
                    ->action(function (ApiSubscriber $record, array $data) {
                        \ApiSubscriberManager::renew_token($record->id, $data['abilities'], true);
                        Notification::make()
                            ->title(__('admin/api_subscriber.action_renew_notification_success_label'))
                            ->success()
                            ->send();
                    })
                    ->form([
                        CheckboxList::make('abilities')
                            ->label(__('admin/api_subscriber.action_renew_abilities_label'))
                            ->options($abilities),
                    ]),

                Action::make(__('admin/api_subscriber.action_disable_label'))
                    ->color('secondary')
                    ->icon('heroicon-o-arrow-circle-down')
                    ->action(function (ApiSubscriber $record, array $data) {
                        \ApiSubscriberManager::remove_token($record->id);
                        Notification::make()
                            ->title(__('admin/api_subscriber.action_disable_notification_success_label'))
                            ->success()
                            ->send();
                    })
                    ->requiresConfirmation()
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => \Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages\ListApiSubscribers::route('/'),
            'create' => \Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages\CreateApiSubscriber::route('/create'),
            'view' => \Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages\ViewApiSubscriber::route('/{record}'),
            'edit' => \Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages\EditApiSubscriber::route('/{record}/edit'),
        ];
    }
}
