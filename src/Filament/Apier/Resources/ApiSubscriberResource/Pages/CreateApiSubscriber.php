<?php

namespace Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages;

use Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Database\Eloquent\Model;

class CreateApiSubscriber extends CreateRecord
{
    protected static string $resource = ApiSubscriberResource::class;

    protected function handleRecordCreation(array $data): Model
    {
        $subscriber = \ApiSubscriberManager::create_with_token(
            $data['name'],
            $data['contact_name'],
            $data['contact_email'],
            $data['contact_phone'],
            $data['description'],
            $data['abilities'],
            true,
        );

        return $subscriber;
    }
}
