<?php

namespace Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages;

use Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditApiSubscriber extends EditRecord
{
    protected static string $resource = ApiSubscriberResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
