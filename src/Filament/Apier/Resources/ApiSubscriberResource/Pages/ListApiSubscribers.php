<?php

namespace Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages;

use Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListApiSubscribers extends ListRecords
{
    protected static string $resource = ApiSubscriberResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }
}
