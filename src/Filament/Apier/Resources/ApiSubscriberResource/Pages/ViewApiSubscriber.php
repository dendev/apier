<?php

namespace Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource\Pages;

use Dendev\Apier\Filament\Apier\Resources\ApiSubscriberResource;
use Filament\Resources\Pages\ViewRecord;

class ViewApiSubscriber extends ViewRecord
{
    protected static string $resource = ApiSubscriberResource::class;
}
