<?php

namespace Dendev\Apier\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Dendev\Apier\Traits\HasPackageFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;

class ApiSubscriber extends Model
{
    use HasApiTokens, HasPackageFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'contact_name',
        'contact_email',
        'contact_phone',
        'description',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'abilities' => 'array',
    ];

    protected $appends = [
        'has_token',
        'abilities',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    /*
    public function has_token()
    {
        return ! $this->tokens->isEmpty();
    }
    */

    public function routeNotificationForMail($notification)
    {
        return [$this->contact_email => $this->contact_name];
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function token()
    {
        return $this->hasOne(PersonalAccessToken::class, 'tokenable_id', 'id')->where('tokenable_type', '=', 'Dendev\Apier\Models\ApiSubscriber');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function abilities(): Attribute
    {
        $abilities = false;

        $token = $this->token;
        if( $token )
        {
            $abilities = $this->token->abilities;

            if( is_array($abilities ) && count($abilities) == 1 && $abilities[0] == '*' )
                $abilities = config('api_subscriber.abilities');
        }

        return Attribute::make(
            get: fn( $value) => $abilities
        );
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    protected function hasToken() : Attribute
    {
        return Attribute::make(
            get: fn($value) => ! $this->tokens->isEmpty()
        );
    }

}
