<?php

namespace Dendev\Apier\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsChannel;
use NotificationChannels\MicrosoftTeams\MicrosoftTeamsMessage;

class ApiSubscriberTokenSet extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return [MicrosoftTeamsChannel::class, 'mail'];
    }

    public function toMail($notifiable): MailMessage
    {
        $values = $this->toArray($notifiable);

        $subscriber_url = $values['subscriber_url'];
        $subscriber_name = $values['subscriber_name'];
        $abilities = $values['abilities'];
        $token_name = $values['token_name'];
        $token = $values['token'];
        $api_base_url = $values['api_base_url'];

        return ( new MailMessage )
            ->subject(__('admin/api_subscriber.notification_mail_subject') )
            ->greeting(__('admin/api_subscriber.notification_mail_greeting') )
            ->line(__('admin/api_subscriber.notification_mail_content', ['url' => $api_base_url, 'token' => $token, 'abilities' => $abilities]) );
    }

    public function toMicrosoftTeams($notifiable)
    {
        $values = $this->toArray($notifiable);

        $subscriber_url = $values['subscriber_url'];
        $subscriber_name = $values['subscriber_name'];
        $abilities = $values['abilities'];
        $token_name = $values['token_name'];
        $token = $values['token'];

        return MicrosoftTeamsMessage::create()
            ->to(config('services.microsoft_teams.sales_url'))
            ->type('success')
            ->title('New SimplePlanning ApiSubscription')
            ->content("Subscriber '$subscriber_name' can now use api with token '$token' who has abilities '$abilities'")
            ->button('Check Subscriber', $subscriber_url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable): array
    {
        $subscriber_url = route('filament.resources.api-subscribers.view', ['record' => $notifiable->id]);
        $subscriber_name = $notifiable->name;
        $subscriber_email = $notifiable->email;
        $abilities = $notifiable->abilities_as_string;
        $token_name = $notifiable->token_name;
        $token = $notifiable->token_as_string;
        $api_documentation_url = route('l5-swagger.default.api');
        $api_base_url = config('url') . '/api';

        return [
            'subscriber_name' => $subscriber_name,
            'subscriber_url' => $subscriber_url,
            'token_name' => $token_name,
            'token' => $token,
            'abilities' => $abilities,
            'api_documentation_url' => $api_documentation_url,
            'api_base_url' => $api_base_url,
        ];
    }
}
