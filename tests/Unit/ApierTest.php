<?php

namespace Dendev\Apier\Tests\Unit;

use Dendev\Apier\Database\Factories\ApiSubscriberFactory;
use Dendev\Apier\Models\ApiSubscriber;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;
use Laravel\Sanctum\NewAccessToken;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class ApierTest extends TestCase
{
    use RefreshDatabase;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $loader = AliasLoader::getInstance();
        $loader->alias('Apier', '\App\Facades\ApierFacade');
    }

    protected function getPackageProviders($app)
    {
        return ['Dendev\Apier\ApierServiceProvider'];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $connection);
    }

    protected function setUp(): void
    {
        parent::setUp();
        ApiSubscriberFactory::guessModelNamesUsing(callback: function(){ return "Dendev\Apier\Models\ApiSubscriber";});
    }

    //
    public function testBasic()
    {
        $exist = \Apier::test_me();
        $this->assertEquals('apier', $exist);
    }

    public function testCreate()
    {

        $values = ApiSubscriber::factory()->make();//->getAttributes();

        $subscriber = \Apier::create(
            $values['name'],
            $values['contact_name'],
            $values['contact_email'],
            $values['contact_phone'],
            $values['description'],
        );

        $this->assertInstanceOf(ApiSubscriber::class, $subscriber);
        $this->assertEquals($values['name'], $subscriber->name);
    }

    public function testCreateTokenForSubscriber()
    {
        $subscriber = ApiSubscriber::factory()->create();

        $token = \Apier::create_token_for_subscriber($subscriber, false, true );

        $this->assertInstanceOf(NewAccessToken::class, $token);
        $this->assertIsString($token->plainTextToken);
    }

    public function testCreateWithToken()
    {
        $values = ApiSubscriber::factory()->make()->getAttributes();
        $abilities = [];
        $send_notify = true;

        $subscriber = \Apier::create_with_token(
            $values['name'],
            $values['contact_name'],
            $values['contact_email'],
            $values['contact_phone'],
            $values['description'],
            $abilities,
            $send_notify,
        );

        $this->assertInstanceOf(ApiSubscriber::class, $subscriber);
        $this->assertEquals($values['name'], $subscriber->name);
        $this->assertIsString($subscriber->token_as_string);
    }

    public function testRemoveToken()
    {
        $values = ApiSubscriber::factory()->make()->getAttributes();
        $abilities = [];
        $send_notify = true;

        $subscriber = \Apier::create_with_token(
            $values['name'],
            $values['contact_name'],
            $values['contact_email'],
            $values['contact_phone'],
            $values['description'],
            $abilities,
            $send_notify,
        );

        $token_1 = $subscriber->token_as_string;
        $subscriber->refresh();

        $this->assertInstanceOf(ApiSubscriber::class, $subscriber);
        $this->assertEquals($values['name'], $subscriber->name);
        $this->assertIsString($token_1);

        $subscriber = \Apier::remove_token($subscriber, $abilities);
        $this->assertEmpty($subscriber->tokens);
    }

    public function testRenewToken()
    {
        $values = ApiSubscriber::factory()->make()->getAttributes();
        $abilities = [];
        $send_notify = true;

        $subscriber = \Apier::create_with_token(
            $values['name'],
            $values['contact_name'],
            $values['contact_email'],
            $values['contact_phone'],
            $values['description'],
            $abilities,
            $send_notify,
        );

        $token_1 = $subscriber->token_as_string;

        $this->assertInstanceOf(ApiSubscriber::class, $subscriber);
        $this->assertEquals($values['name'], $subscriber->name);
        $this->assertIsString($token_1);

        $subscriber = \Apier::renew_token($subscriber, $abilities);
        $token_2 = $subscriber->token_as_string;

        $this->assertIsString($token_2);
        $this->assertNotEquals($token_1, $token_2);
    }
}
